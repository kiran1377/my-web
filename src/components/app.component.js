import React from 'react';
import { Login } from './../components/auth/login/login.component'

//functional components:

export const App = (props) => {
    //incoming arguments for components is props
    //a functional components must have return block and single html node must be returned.

    return (
        <div>
            <p>Welcome to components</p>
            <Login isLoggedIn={true} />
        </div>      //isLoggedIn is a flag using interpolation pattern
    )
}

//aba yeslai index.js file ma export garney which is given above:
//export const App = ...
//ani index.js file lay import garcha thyakkai yei App ko naam lay