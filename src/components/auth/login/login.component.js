import React, { Component } from 'react';

// syntax of class:
//class Name extends  React.Component{}


export class Login extends Component {

    constructor() {
        // this.a = 'hi';
        super();
        console.log('this.props>>', this.props);
        //to maintain data within a component use state:
        this.state = {
            username: '',
            password: '',
            remember_me: false,
            isSubmitting: false     //submit ko lagi flag
        }
    }

    //TODO use life cycle methods to know about when to used promps

    onSubmit = (e) => {     //tala lekhko form ko function call gareko
        e.preventDefault(); //prevent default behaviour
        // console.log('event is >>', e);
    }

    handleChange = (e) => {       //tala lekhko form ko function call gareko
        console.log('e.>>', e.target);
        const { name, value } = e.target;
        console.log('name is>>', name);
        console.log('value is>>', value);
        //now update state on every change event:
        this.setState({
            [name]: value
        })
    }



    render() {
        // render method is mandatory inside class based component
        // render must return single html node
        // try to keep UI logic inside render
        return (
            <div>
                <h2>Login</h2>
                <p>please login to use our app</p>
                <form onSubmit={this.onSubmit} className="form-group">
                    <label htmlFor="username">Username</label>
                    <input type="text" name="username" placeholder="username" id="username" onChange={this.handleChange} className="form-control"></input>
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" placeholder="password" id="password" onChange={this.handleChange} className="form-control"></input>
                    <br/>
                    <button className= "btn btn-primary">Login</button>
                </form>
            </div>
        )
    }
}